package pswholder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.io.IOException;

import javax.swing.*;
import java.awt.event.*;

public class PswH {
private static String psw;
private static boolean bool = false;

private static ArrayList<String> data = new ArrayList();
private static ArrayList<String> encryptedData = new ArrayList();
private static String dFP = "data.txt";	//data file name

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame f=new JFrame("Password Holder");//creating instance of JFrame 
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		JPanel dataGUI = new JPanel(); 
		f.setSize(500,400);	//450 width and 300 height  
		f.setLayout(null);
		f.setVisible(true);
		f.add(contentPane);
		f.add(dataGUI);
		File f1 = new File(dFP);
		
		int i = 0;
		dataInsertGui(dataGUI, i); //prova pagina
		f.remove(contentPane);
		/*
		if(!f1.exists()) {
			CreateFile(contentPane);
			AES256 KeySes = new AES256(psw);
			f.remove(contentPane); 	//contentPane.removeAll();
			f.add(dataGUI);
			while
			dataInsertGui(dataGUI, i);	//richiama funzione per dataGUI
			
			encryptAll(KeySes);
			
		}else {
			readData(f1);
			
			
			
		}
		*/

	}
	
	
	private static void CreateFile(JPanel f) { 
		File f1 = new File(dFP);
		try {
			f1.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		//create the psw file and go on
		final JPasswordField jpField = new JPasswordField(); 
		final JPasswordField jpField2 = new JPasswordField();
		
		JLabel l1=new JLabel("Password:");
		JLabel l4=new JLabel("Repeat:");
		JLabel l2=new JLabel("Insert the password ");
		JLabel l3=new JLabel("Repeat to ensure procedure");
		JButton b=new JButton("");//creating instance of JButton 
		b.setBounds(250,120,100, 40);//x axis, y axis, width, height 
		l2.setBounds(20,20,400,30);
		l3.setBounds(20,40,400,30);
		l1.setBounds(20,100, 80,30);  
		l4.setBounds(20,150, 80,30);
		jpField.setBounds(100,100,100,30); 
		jpField2.setBounds(100,150,100,30);
		f.add(jpField); 
		f.add(jpField2); 
		f.add(l2);
		f.add(l3);
		f.add(l1); 
		f.add(l4);
		f.add(b);//adding button in JFrame        
		f.setSize(450,300);	//600 width and 400 height  
		f.setLayout(null);	//using no layout managers  
		f.setVisible(true);	//making the frame visible 
		
	    b.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){  
	    		String psw1 = new String (jpField.getPassword());
	    		String psw2 = new String (jpField2.getPassword());
    	        if(psw1 == psw2)// action to do on click
    	        	psw = new String(psw1);	
    	    }  
	    });        
	}
	
	private static void WelcomP(JFrame f, String psw) { 
		final JPasswordField jpField = new JPasswordField(); 
		JLabel l1=new JLabel("Password:");   
		JButton b=new JButton("Log-in");//creating instance of JButton 
		
		b.setBounds(250,100,100, 40);//x axis, y axis, width, height 
		l1.setBounds(20,100, 80,30);    
		jpField.setBounds(100,100,100,30); 
		
		f.add(jpField);  
		f.add(l1);  
		f.add(b);//adding button in JFrame  
	             
		f.setSize(600,400);	//600 width and 400 height  
		f.setLayout(null);	//using no layout managers  
		f.setVisible(true);	//making the frame visible 
		
	    b.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){ 
	    		char[] text = jpField.getPassword();
	    	/*	if (PswCheck(text) == 1) {	// action to do on click 
	    			psw = new String();
	    		} */
	    	} 
	    });  
	    
	}
	
	private static void encryptAll(AES256 key) {
		int i;
		String tmp = "";
		String e = "";
		for(i = 0; i < data.size(); i++) {
			tmp = data.get(i);
			e = key.encrypt(tmp);
			encryptedData.add(e);
		}
		
	}
	
	private static void decryptAll(AES256 key) {
		int i;
		String tmp = "";
		String e = "";
		for(i = 0; i < encryptedData.size(); i++) {
			tmp = encryptedData.get(i);
			e = key.decrypt(tmp);
			data.add(e);
		}
		
	}

	private static void writeData(File fout) {
		
	}
	
	private static void readData(File f1) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(f1));
			String line;
		    while ((line = br.readLine()) != null) {
		    	encryptedData.add(line);
		    }
		       // process the line
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static void dataInsertGui(JPanel dataGUI,int i) {
        dataGUI.setSize(500, 400);      
        JLabel prof = new JLabel("Profile:");
        JLabel unm = new JLabel("UN or email:");
        JLabel psw1 = new JLabel("Password:");
        JButton okButton = new JButton("Add");
        JTextField profile = new JTextField();
        JTextField uname = new JTextField();
        JTextField password = new JTextField();
        JLabel label = new JLabel("Profile n." + i+1);	//display the n. of account that is 1 more than the arraylist index
        prof.setBounds(20,100,80,30);  //x asses , y, width, height
        unm.setBounds(20,150,80,30);
        psw1.setBounds(20,200,80,30);
        profile.setBounds(200,100,150,30);
        uname.setBounds(200,150,150,30);
        password.setBounds(200,200,150,30);
        okButton.setBounds(200, 300, 50, 30);
        dataGUI.add(profile);
        dataGUI.add(uname);
        dataGUI.add(password);
        dataGUI.add(prof);
        dataGUI.add(unm);
        dataGUI.add(psw1);
        dataGUI.add(label);
        dataGUI.add(okButton);
        dataGUI.setLayout(null);
        dataGUI.setVisible(true); 
        okButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
        	
        	onClick(profile.getText(),uname.getText(),password.getText());
       
        }
        });

       
    }
	
	public static void onClick(String input1, String input2, String input3) {
		data.add(input1);
        data.add(input2);
        data.add(input3);
	}
}
