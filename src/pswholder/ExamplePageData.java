package pswholder;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class ExamplePageData {

	private JFrame frame;
	private JTextField txtPampabello;
	private JTextField txtSitantobello;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ExamplePageData window = new ExamplePageData();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ExamplePageData() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 224, 301);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("NETFLIX");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblNewLabel.setBounds(10, 0, 191, 70);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Username");
		lblNewLabel_1.setBounds(10, 70, 74, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		txtPampabello = new JTextField();
		txtPampabello.setText("PampaBello");
		txtPampabello.setBounds(10, 90, 122, 20);
		frame.getContentPane().add(txtPampabello);
		txtPampabello.setColumns(10);
		
		JLabel lblNewLabel_1_1 = new JLabel("Password");
		lblNewLabel_1_1.setBounds(10, 120, 74, 14);
		frame.getContentPane().add(lblNewLabel_1_1);
		
		txtSitantobello = new JTextField();
		txtSitantobello.setText("SiTantoBello");
		txtSitantobello.setColumns(10);
		txtSitantobello.setBounds(10, 140, 122, 20);
		frame.getContentPane().add(txtSitantobello);
		
		btnNewButton = new JButton("Back");
		btnNewButton.setBounds(10, 228, 74, 23);
		frame.getContentPane().add(btnNewButton);
	}
}
